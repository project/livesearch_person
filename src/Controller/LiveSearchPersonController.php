<?php

namespace Drupal\livesearch_person\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\livesearch_person\LiveSearchPersonInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class LiveSearchPersonController.
 */
class LiveSearchPersonController extends ControllerBase {

  /**
   * Drupal\livesearch_person\LiveSearchPersonInterface definition.
   *
   * @var \Drupal\livesearch_person\LiveSearchPersonInterface
   */
  protected $livesearchWebapi;

  /**
   * Constructs a new LiveSearchPerson object.
   */
  public function __construct(LiveSearchPersonInterface $livesearch_webapi) {
    $this->livesearchWebapi = $livesearch_webapi;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('livesearch_person.webapi')
    );
  }

  /**
   * Search directory.
   */
  public function searchDirectory(Request $request) {
    $data = [
      'results' => [],
    ];
    try {
      $string = $request->request->get('string', '');
      if (trim($string)) {
        $results = $this->livesearchWebapi->getDirectory($string);
        $data['results'] = $results;
      }
    }
    catch (\Exception $e) {
      // Do nothing.
    }

    return new JsonResponse($data);
  }

}
