<?php

namespace Drupal\livesearch_person\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class LiveSearchConfigForm.
 */
class LiveSearchConfigForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'livesearch_person.livesearchconfig',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'livesearch_person_config_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('livesearch_person.livesearchconfig');

    $form['livesearch_url'] = [
      '#type' => 'url',
      '#title' => $this->t('Livesearch url'),
      '#default_value' => $config->get('livesearch_url'),
      '#required' => TRUE,
      '#description' => $this->t('eg: https://search.datafactory.online/person/'),
    ];

    $form['livesearch_apikey'] = [
      '#type' => 'textfield',
      '#title' => $this->t('API Key'),
      '#default_value' => $config->get('livesearch_apikey'),
      '#required' => TRUE,
    ];

    $form['livesearch_debug_javascript'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Debug javascript'),
      '#default_value' => $config->get('livesearch_debug_javascript'),
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);
    $config = $this->config('livesearch_person.livesearchconfig');
    $config
      ->set('livesearch_url', $form_state->getValue('livesearch_url'))
      ->set('livesearch_debug_javascript', $form_state->getValue('livesearch_debug_javascript'));
    if ($apiKey = $form_state->getValue('livesearch_apikey')) {
      $config->set('livesearch_apikey', $apiKey);
    }
    $config->save();
  }

}
