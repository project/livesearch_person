<?php

namespace Drupal\livesearch_person\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Messenger\MessengerTrait;
use Drupal\livesearch_person\LiveSearchPersonInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class LiveSearchPersonTestConnectionForm.
 */
class LiveSearchPersonTestConnectionForm extends FormBase {
  use MessengerTrait;
  /**
   * Drupal\livesearch_person\LiveSearchPersonInterface definition.
   *
   * @var \Drupal\livesearch_person\LiveSearchPersonInterface
   */
  protected $livesearchWebapi;
  /**
   * Constructs a new LiveSearchPersonTestConnectionForm object.
   */
  public function __construct(LiveSearchPersonInterface $livesearch_webapi) {
    $this->livesearchWebapi = $livesearch_webapi;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('livesearch_person.webapi')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'livesearch_person_test_connection_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['search'] = [
      '#type'      => 'textfield',
      '#title'     => $this->t('Search Directory'),
      '#maxlength' => 255,
      '#size'      => 64,
      '#required'  => TRUE,
    ];

    $form['submit'] = [
      '#type'  => 'submit',
      '#value' => $this->t('Submit'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $config = \Drupal::config('livesearch_person.livesearchconfig');
    $url = $config->get('livesearch_url');
    if (!$url) {
      $form_state->setErrorByName(
        '',
        $this->t('A livesearch person url/endpoint has not been configured.')
      );
    }
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    try {
      $result = $this->livesearchWebapi->getDirectory(
        $form_state->getValue('search')
      );

      $this->messenger()->addMessage(
        $this->t(
          'Webservice response: <pre>%data</pre>',
          ['%data' => print_r($result, TRUE)]
        )
      );
    }
    catch (\Exception $e) {
      \Drupal::messenger()->addError(
        $this->t(
          'An error has occurred: %message',
          ['%message' => $e->getMessage()]
        )
      );
    }

  }

}
