<?php

namespace Drupal\livesearch_person;

use GuzzleHttp\ClientInterface;
use GuzzleHttp\RequestOptions;
use Psr\Http\Message\ResponseInterface;

class LiveSearchPersonService implements LiveSearchPersonInterface {

  /**
   * GuzzleHttp\ClientInterface definition.
   *
   * @var \GuzzleHttp\ClientInterface
   */
  protected $httpClient;

  /**
   * The livesearch url.
   *
   * @var string
   */
  protected $url;

  /**
   * API Key.
   *
   * @var string
   */
  protected $apiKey;

  /**
   * Constructs a new LiveSearchPersonService object.
   *
   * @param \GuzzleHttp\ClientInterface $http_client
   *   Guzzle HTTP Client.
   */
  public function __construct(ClientInterface $http_client) {
    $this->httpClient = $http_client;
    $config = \Drupal::config('livesearch_person.livesearchconfig');
    $this->url = $config->get('livesearch_url');
    $this->apiKey = $config->get('livesearch_apikey');
  }


  /**
   * Search in Directory service.
   *
   * @param string $search
   *   Search parameter.
   *
   * @return array
   *   Return array of search result.
   */
  public function getDirectory($search): array {
    $uri = $this->url;
    $response = $this->get($uri, $search);
    $json = (string) $response->getBody();
    $result = json_decode($json, FALSE);
    if ($result) {
      return $this->buildValues($result);
    }

    return [];
  }

  /**
   * Build values that will be used by webform.
   *
   * @param array $results
   *   Result array for bulding the webform.
   *
   * @return array
   *   Return webform.
   */
  protected function buildValues(array $results): array {
    if (empty($results)) {
      return [];
    }
    foreach ($results as $result) {
      $this->buildFullName($result);
      $this->buildFirstMiddleName($result);
      $this->buildLastName($result);
      $this->buildFinalAddress($result, $include_city = FALSE);
      $this->buildCity($result);
      $this->buildZipCode($result);
      $this->buildDateBirth($result);
    }
    return $results;
  }

  /**
   * Build the date of birth.
   */
  protected function buildDateBirth(\stdClass $result) {
    if ($result->personDetail->birthDate) {
      $result->date_birth = $result->personDetail->birthDate;
    }
    else {
      $result->date_birth = '';
    }
  }

  /**
   * Build the full name.
   */
  protected function buildFirstMiddleName(\stdClass $result) {
    if ($result->names[0]->firstName || $result->names[0]->middleName) {
      $result->firstname = $result->names[0]->firstName . ' ' . $result->names[0]->middleName;
    }
  }


  /**
   * Build the full name.
   */
  protected function buildLastName(\stdClass $result) {
    if ($result->names[0]->lastName) {
      $result->lastname = $result->names[0]->lastName;
    }
  }

  /**
   * Build the full name.
   */
  protected function buildFullName(\stdClass $result) {
    if ($result->names[0]->fullName) {
      $result->fullname = $result->names[0]->fullName;
    }
  }

  /**
   * Build the final address.
   */
  protected function buildFinalAddress(\stdClass $result, bool $include_city = TRUE) {
    if ($result->addresses[0]->address) {
      $result->final_address = $result->addresses[0]->address;
    }
    if ($include_city && $result->addresses[0]->city) {
      $result->final_address .= ', ' . $result->addresses[0]->city;
    }
  }
  /**
   * Build the city.
   */
  protected function buildCity(\stdClass $result) {
    if ($result->addresses[0]->city) {
      $result->city = $result->addresses[0]->city;
    }
  }

  /**
   * Build the zipcode.
   */
  protected function buildZipCode(\stdClass $result) {
    if ($result->addresses[0]->postalCode) {
      $result->zipcode = $result->addresses[0]->postalCode;
    }
  }

  /**
   * Request a GET to the webservices.
   *
   * @param string $uri
   *   URL.
   * @param string $search
   *   Search string.
   * @param array $options
   *   Options array.
   *
   * @return \Psr\Http\Message\ResponseInterface
   *   Return response object.
   */
  protected function get($uri, $search): ResponseInterface {

    return $this->httpClient->get(
      $uri . $search,
      [
        'headers'            => [
          'Content-Type' => 'application/json',
          'X-API-Key' => $this->apiKey,
        ],
      ]
    );
  }

  /**
   * Get the fields to be mapping.
   */
  public static function fieldsMapping(): array {
    return [
      'fullname'      => t('Full name'),
      'firstname'     => t('First name'),
      'lastname'      => t('Last name'),
      'final_address' => t('Address'),
      'date_birth'    => t('Date of birth'),
      'zipcode'       => t('Zip code'),
      'city'          => t('City'),
    ];
  }
}
