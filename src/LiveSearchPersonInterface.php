<?php

namespace Drupal\livesearch_person;

interface LiveSearchPersonInterface {

  /**
   * Search in Directory service.
   *
   * @param string $search
   *   Search string to be passed here.
   *
   * @return array
   *   Return array of directories.
   */
  public function getDirectory($search): array;

  /**
   * Get the fields to be mapping.
   */
  public static function fieldsMapping(): array;

}
